Sécurité des applications web
=============================

Installation
------------
Sous Debian:

```
sudo apt-get install apache2 mysql-server mysql-client php5 php5-mysql
```


Configuration
-------------

### Base de données

Pour créer la base de données:

```
$ service mysql start
$ mysql
mysql> CREATE DATABASE secu_web;
mysql> USE secu_web;
mysql> CREATE TABLE IF NOT EXISTS `users` (
          `id` int UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
          `login` varchar(15) UNIQUE NOT NULL,
          `password` varchar(65) NOT NULL
        );
mysql> CREATE TABLE IF NOT EXISTS `posts` (
          `id` int UNSIGNED AUTO_INCREMENT PRIMARY KEY,
          `author` int UNSIGNED NOT NULL,
          `title` varchar(200) NOT NULL,
          `content` varchar(10000) NOT NULL,
          FOREIGN KEY (author) REFERENCES users(id) ON DELETE CASCADE
        );
mysql> CREATE USER 'secu_web'@'localhost' IDENTIFIED BY 'secu_web';
mysql> GRANT ALL ON secu_web.* TO 'secu_web'@'localhost'; 
```


Lancement
---------
Pour démarrer Apache, au choix:

* `service apache2 start`
* `systemctl apache2 start`
* `apache2ctl start`

Idem avec MySQL:

* `service mysql start`
