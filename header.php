<?php
session_start();
require_once("utils.php");
?>

<p>
    <ul>
        <li><a href="index.php">Accueil</a></li>
<?php

if (isset($_SESSION['login']))
    echo "<li><a href='posts.php?action=new'>Créer un article</a>";

?>
        <li><a href="posts.php">Liste des articles</a></li>
        <li><a href="users.php">Liste des utilisateurs</a></li>
<?php

if (isset($_SESSION['login']))
    echo "<li><a href='disconnect.php'>Se déconnecter</a>";
else {
    echo "<li><a href='register.php'>Créer un compte</a></li>";
    echo "<li><a href='login.php'>Login</a></li>";
}

?>
    </ul>
</p>

<?php

if (isset($_SESSION['login']))
    echo success("Connected as ".htmlspecialchars($_SESSION['login'], ENT_QUOTES, 'UTF-8'));
else
    echo "<p>Not connected.</p>";

?>

<hr/>
