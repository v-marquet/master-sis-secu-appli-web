<?php

// cf http://php.net/manual/fr/function.session-destroy.php

session_start();

$_SESSION = array();  // Détruit toutes les variables de session

if (ini_get("session.use_cookies")) {
    $params = session_get_cookie_params();
    setcookie(session_name(), '', time() - 42000,
        $params["path"], $params["domain"],
        $params["secure"], $params["httponly"]
    );
}

session_destroy();

?>


<html>
    <head>
        <title>Blog</title>
    </head>

    <body>
<?php require_once("header.php") ?>

        <h1>Déconnexion</h1>

<?php

require_once("utils.php");
echo success("Vous avez été déconnecté.");

?>

    </body>
</html>
