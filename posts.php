<html>
    <head>
        <title>Blog</title>
    </head>

    <body>
<?php require_once("header.php") ?>

        <h1>Articles</h1>


<?php

// ini_set('display_errors', 1);
// error_reporting(E_ALL);

require_once("utils.php");
session_start();

mysql_connect("localhost", "secu_web", "secu_web")
    or die(error("impossible de se connecter à la BDD: ".mysql_error()));

mysql_select_db("secu_web")
    or die(error("database 'secu_web' not found."));


if (isset($_GET['action']) && $_GET['action'] == "new") {
    // if it's a POST request, we create a new blog post
    if (isset($_POST['title']) && is_string($_POST['title'])
        && isset($_POST['content']) && is_string($_POST['content'])) {

        if (isset($_SESSION['login'])) {
            // the blog post belongs to the user, wo we must find the user ID
            // in the database before creating the blog post
            $query = sprintf("SELECT * FROM users WHERE login = '%s'", $_SESSION['login']);

            $result = mysql_query($query) or die(error("invalid query: ".mysql_error()));
            $user = mysql_fetch_array($result);
            $user_id = $user['id'];

            $query = sprintf("INSERT INTO posts (title, content, author) VALUES('%s', '%s', '%s')",
                mysql_real_escape_string($_POST['title']),
                mysql_real_escape_string($_POST['content']),
                mysql_real_escape_string($user_id)
            );
            mysql_query($query) or die(error("invalid query:".mysql_error()));

            echo success("Article publié avec succès.");
        }
        else
            die(error("Vous devez être connecté pour créer un article."));

    }
    // else, we display the form
    else {
        if (!isset($_SESSION['login']))
            echo error("Cet article ne pourra pas être sauvegardé car vous n'êtes pas connecté.");

        echo "<form method='POST'>";
        echo "    Titre: <br/>  <input type='text' name='title' size='75px' maxlength='200'><br/><br/>";
        echo "    Corps: <br/>  <textarea name='content' rows='20' cols='100' maxlength='10000'></textarea><br/><br/>";
        echo "    <input type='submit' value='submit'>";
        echo "</form>";
    }
}
else if (isset($_GET['action']) && $_GET['action'] == "edit"
    && isset($_GET['id']) && is_numeric($_GET['id'])) {
    
    // first we check that an article with this ID exists in the database
    $query = sprintf("SELECT * FROM posts WHERE id = '%s'", mysql_real_escape_string($_GET['id']));
    $result = mysql_query($query) or die(error("invalid query: ".mysql_error()));
    if (mysql_num_rows($result) == 0)
        die(error("Il n'y a pas d'article ave cet ID."));
    $post = mysql_fetch_array($result);

    // then, we check that the user logged in has the right to edit it
    if (isset($_SESSION['login'])) {
        $query = sprintf("SELECT * FROM users WHERE login = '%s'", $_SESSION['login']);
        $result = mysql_query($query) or die(error("invalid query: ".mysql_error()));
        $user = mysql_fetch_array($result);

        if ($user['id'] != $post['author'])
            die(error("Vous ne pouvez pas éditer les articles d'un autre utilisateur."));
    }
    else
        die(error("Vous ne pouvez pas éditer un article sans être connecté."));

    // if some data is POSTed, we update the database
    if (isset($_POST['title']) && is_string($_POST['title'])
        && isset($_POST['content']) && is_string($_POST['content'])) {

        $query = sprintf("UPDATE posts SET title='%s', content='%s' WHERE id = '%s'",
            mysql_real_escape_string($_POST['title']),
            mysql_real_escape_string($_POST['content']),
            mysql_real_escape_string($_GET['id'])
        );
        $result = mysql_query($query) or die(error("invalid query: ".mysql_error()));

        echo succes("Article mis à jour avec succès.");
    }
    // otherwise, we display a form
    else {
        echo "<form method='POST' action='?action=default'>";
        $title = htmlspecialchars($post['title'], ENT_QUOTES, 'UTF-8');
        echo "    Titre: <br/>  <input type='text' name='title' size='75px' maxlength='200' value='".$title."'><br/><br/>";
        $content = htmlspecialchars($post['content'], ENT_QUOTES, 'UTF-8');
        echo "    Corps: <br/>  <textarea name='content' rows='20' cols='100' maxlength='10000'>".$content."</textarea><br/><br/>";
        echo "    <input type='submit' value='submit'>";
        echo "</form>";
    }
}
else {
    // we get the current logged in user, we need his ID to know which posts he can edit
    if (isset($_SESSION['login'])) {
        $query = sprintf("SELECT * FROM users WHERE login = '%s'", $_SESSION['login']);
        $result = mysql_query($query) or die(error("invalid query: ".mysql_error()));
        $user = mysql_fetch_array($result);
    }

    // we display all blog posts
    $query = "SELECT * FROM posts";
    $result = mysql_query($query) or die(error("invalid query: ".mysql_error()));

    echo "<p>Nombre d'articles: ".mysql_num_rows($result)."</p>";

    while ($post = mysql_fetch_array($result)) {
        echo "<div style='border:1px solid black;'>";
        echo "<p><b>".htmlspecialchars($post['title'], ENT_QUOTES, 'UTF-8')."</b></p>";
        echo "<p>".htmlspecialchars($post['content'], ENT_QUOTES, 'UTF-8')."</p>";

        // we get author name
        $query = sprintf("SELECT * FROM users WHERE id = '%s'", $post['author']);
        $result2 = mysql_query($query) or die(error("invalid query: ".mysql_error()));
        $author = mysql_fetch_array($result2);
        echo "<p><i>Auteur: ".htmlspecialchars($author['login'], ENT_QUOTES, 'UTF-8')."</i></p>";

        if (isset($_SESSION['login'])) {
            if ($user['id'] == $author['id']) {
                echo "<p><a href='?action=edit&id=".$post['id']."'>modifier</a></p>";
            }
        }

        echo "</div><br/>";
    }
}


?>


    </body>
</html>